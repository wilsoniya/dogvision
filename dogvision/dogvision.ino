#define TRIGGER_PIN 6
#define ECHO_PIN 5
#define SPKR_PIN 4


unsigned long echo_rising_time_us = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(TRIGGER_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(SPKR_PIN, OUTPUT);

  digitalWrite(TRIGGER_PIN, LOW);

  Serial.begin(9600);

  //  attachInterrupt(digitalPinToInterrupt(ECHO_PIN), echo_level_change, CHANGE);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(TRIGGER_PIN, HIGH);
  //digitalWrite(SPKR_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  //digitalWrite(SPKR_PIN, LOW);

  unsigned long duration_us = pulseIn(ECHO_PIN, HIGH);
  float dist_cm = duration_us / 58.0;
  unsigned int freq = 3000.0 / dist_cm;

  if (dist_cm < 400 && freq > 15) {
    unsigned int freq = 7000.0 / dist_cm;
    //Serial.println(freq, DEC);
    tone(SPKR_PIN, freq);
  } else {
    noTone(SPKR_PIN);
  }

  //Serial.println(dist_cm, DEC);


  delay(50);
}

//void echo_level_change() {
//  unsigned long time_us = micros();
//  char level = digitalRead(ECHO_PIN);
//
//  switch (level) {
//  case 0:
//  {
//    unsigned long delta = time_us - echo_rising_time_us;
//    echo_rising_time_us = 0;
//    Serial.println(delta, DEC);
//    break;
//  }
//  case 1:
//    echo_rising_time_us = time_us;
//    break;
//  }
//}
