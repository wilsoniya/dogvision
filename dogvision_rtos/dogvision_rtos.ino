#include <Arduino_FreeRTOS.h>

#define ECHO_PIN 10
#define TRIGGER_PIN 11
#define LED_PIN 13

#define US_PER_MM 5.787
#define BLINK_PERIOD_MS 20

#define limit 2000.0
#define _max 1000.0
#define _min 20.0
#define threshold 3.0
#define skinniness 5.0
#define offset 160.0
#define limit_over_threshold (limit / threshold)
#define limit_over_skinniness (limit / skinniness)
#define range (_max - _min)

unsigned int DIST_MM = 0;

void setup() {
  Serial.begin(9600);

  xTaskCreate(
    TaskRangefinder,
    (const portCHAR *) "EchoListener",  // human readable
    128,                                // stack size
    NULL,
    1,                                  // priority
    NULL
  );

  xTaskCreate(
    TaskBlinker,
    (const portCHAR *) "Blinker",       // human readable
    128,                                // stack size
    NULL,
    1,                                  // priority
    NULL
  );

  // Now the task scheduler, which takes over control of scheduling individual
  // tasks, is automatically started.
}

void loop() {}

inline void sleep_ms(unsigned int duration_ms) {
    vTaskDelay(duration_ms / portTICK_PERIOD_MS);
}

inline unsigned int get_delay_ms(unsigned int dist_mm) {
    float x = dist_mm - offset;
    return range * (1 + tanh((x - limit_over_threshold) / limit_over_skinniness)) / 2.0;

}

inline void blink() {
    digitalWrite(LED_PIN, HIGH);
    sleep_ms(BLINK_PERIOD_MS);
    digitalWrite(LED_PIN, LOW);
}

void TaskBlinker (void *pvParameters) {
    (void) pvParameters;
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, LOW);

    blink();
    unsigned long last_blink = millis();

    for (;;) {
        unsigned long next_blink = last_blink + get_delay_ms(DIST_MM);
        unsigned long time = millis();

        if (time >= next_blink) {
            blink();
            last_blink = millis();
        }

        sleep_ms(20);
    }
}

void TaskRangefinder(void *pvParameters) {
    (void) pvParameters;

    pinMode(ECHO_PIN, INPUT);
    pinMode(TRIGGER_PIN, OUTPUT);

    digitalWrite(TRIGGER_PIN, LOW);

    for (;;) {
        taskENTER_CRITICAL();
        digitalWrite(TRIGGER_PIN, HIGH);
        delayMicroseconds(20);
        digitalWrite(TRIGGER_PIN, LOW);
        taskEXIT_CRITICAL();

        unsigned long duration_us = pulseIn(ECHO_PIN, HIGH);
        DIST_MM = (unsigned int) (duration_us / US_PER_MM);
        sleep_ms(250);
    }
}
